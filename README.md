# <div align="center"> API Weather Forecast </div>

<div align="center">
<p>🚧 It is in Development 🚧</p>
</br>
<p></p>
</div>

## Technologies used in the project

- Typescript
- NodeJS
- OvernightJs
- Jest

## Technologies used outside the project

- Docker
- insominia

## How to run the project

- Clone this repository

```shell
git clone https://github.com/guilhermelinosx/api-weather-forecast.git
```
- Start the Application in Development

```shell
yarn dev
```