import { StormGlass } from './stormGlass'
import axios from 'axios'
import stormGlassWeather3HoursFixture from './../../test/fixtures/stormglass_weather_3_hours.json'
import stormGlassNormalized3HoursFixture from './../../test/fixtures/stormglass_normalized_response_3_hours.json'

jest.mock('axios')

describe('StormGlass client', () => {
	const mockedAxios = axios as jest.Mocked<typeof axios>

	it('should return the normalized forecast from the StormGlass service', async () => {
		const lat = -33.792726
		const lng = 151.289824

		mockedAxios.get.mockResolvedValue({ data: stormGlassWeather3HoursFixture })

		const stormGlass = new StormGlass(mockedAxios)
		expect(await stormGlass.fetchPoints(lat, lng)).toEqual(stormGlassNormalized3HoursFixture)
	})

	it('should exclude incomplete data points', async () => {
		const lat = -33.792726
		const lng = 151.289824
		const incompleteResponde = {
			hours: [
				{
					windDirection: {
						noaa: 300
					},
					time: '2020-04-26T01:00:00+00:00'
				}
			]
		}

		mockedAxios.get.mockResolvedValue({ data: incompleteResponde })

		const stormGlass = new StormGlass(mockedAxios)

		expect(await stormGlass.fetchPoints(lat, lng)).toEqual([])
	})

	it('should get a generic error from StormGlass service when the request fail before reaching the service', async () => {
		const lat = -33.792726
		const lng = 151.289824

		mockedAxios.get.mockResolvedValue({ message: 'Network Error' })

		const stormGlass = new StormGlass(mockedAxios)

		await expect(stormGlass.fetchPoints(lat, lng)).rejects.toThrow(
			'Unexpected error when trying to communicate to StormGlass: Network Error'
		)
	})
})
