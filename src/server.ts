import { Server } from '@overnightjs/core'
import bodyParser from 'body-parser'
import { Application } from 'express'
import { ForecastController } from './controllers/forecast'

export class SetupServer extends Server {
	constructor(private readonly port = 3001) {
		super()
	}

	public init(): void {
		this.superExpress()
		this.setupControllers()
	}

	private superExpress(): void {
		this.app.use(bodyParser.json())
	}

	private setupControllers(): void {
		const forecastController = new ForecastController()
		this.addControllers([forecastController])
	}

	public getApp(): Application {
		return this.app
	}
}
